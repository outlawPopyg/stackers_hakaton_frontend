import React from 'react';
import ReactDOM from 'react-dom/client';
import App from "./components/App";
import { Provider } from "react-redux";
import { store } from "./store";
import { BrowserRouter as Router,Route, Routes } from "react-router-dom";
import Sidebar from "./components/sidebar/Sidebar";
import Events from "./components/events/Events";
import "./app.css";
import CourseReg from "./components/courseRegistration/CourseReg";
import Chat from "./components/chat/Chat";
import Rating from "./components/rating/Rating";

const root = ReactDOM.createRoot(document.getElementById('root'));
document.store = store;
root.render(
	<Provider store={store}>
		<Router>
			<div className={"app"}>
				<Sidebar />
				<section className={"home-section"}>
					<div style={{ marginLeft: "40px", marginTop: "40px" }}>
						<Routes>
							<Route path={"/"} element={<App />} />
							<Route path={"/events"} element={<Events />} />
							<Route path={"/reg_course/:id"} element={<CourseReg /> } />
							<Route path={"/chats/general/:id"} element={<Chat />} />
							<Route path={"/rating"} element={<Rating />} />
						</Routes>
					</div>

				</section>
			</div>

		</Router>
	</Provider>
);


import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {addAll} from "../../store/events/eventsReducer";
import OutlinedCard from "../card/OutlinedCard";

export default function Events(props) {

	const events = useSelector(state => state.events);
	const dispatch = useDispatch();

	const getAllEvents = async () => {
		try {
			const json = await fetch("http://localhost:8080/events")
				.then(data => data.json())
				.then(res => dispatch(addAll(res)));

		} catch (e) {
			console.error(e);
		}
	}

	useEffect(() => {
		getAllEvents().then();
	}, []);

	return (
		<div>
			{
				events.map(event => {
					return (
						<div key={event.id} style={{ marginBottom: "14px"}}>
							<OutlinedCard {...event} />
						</div>

					)
				})
			}
		</div>

	);
}
import {useNavigate, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import * as React from 'react';
import TextField from '@mui/material/TextField';
import {useEffect, useState} from "react";
import SaveIcon from '@mui/icons-material/Save';
import {LoadingButton} from "@mui/lab";
import {setUser} from "../../store/user/userReducer";



export default function CourseReg(props) {
	const redirect = useNavigate();
	const user = useSelector(state => state.users);
	const [name, setName] = useState();
	const [surname, setSurname] = useState();
	const [patronymic, setPatronymic] = useState();
	const [email, setEmail] = useState();
	const [educationOrganization, setEducationOrganization] = useState();
	const {id} = useParams();
	const [isLoading, setLoading] = useState(false);
	const dispatch = useDispatch();

	if (user.isAuth) {
		redirect("/");
		return;
	}

	const handleSubmit = (e) => {
		e.preventDefault();

		const data = JSON.stringify({
			name,
			surname,
			patronymic,
			email,
			educationOrganization,
			eventId: id
		});

		setLoading(true);
		fetch("http://localhost:8080/signUpAnonim", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: data
		}).then(response => response.json())
			.then(data => {
				console.log(data);
				dispatch(setUser(data));
				setLoading(false);
			}).catch(e => {
				setLoading(false);
		})
	}

	return (
		<div style={{ width: "25%", margin: "0 auto"}}>

			<form onSubmit={handleSubmit}>
				<TextField
					onInput={(e) => setName(e.target.value)}
					required
					id="name"
					label="Имя"
					type="text"
					variant="filled"
					style={{ marginBottom: "1rem"}}
				/>
				<br/>

				<TextField
					onInput={(e) => setSurname(e.target.value)}
					required
					id="surname"
					label="Фамилия"
					type="text"
					variant="filled"
					style={{ marginBottom: "1rem"}}
				/>

				<TextField
					onInput={(e) => setPatronymic(e.target.value)}
					required
					id="patronymic"
					label="Отчество"
					type="text"
					variant="filled"
					style={{ marginBottom: "1rem"}}
				/>

				<TextField
					onInput={(e) => setEmail(e.target.value)}
					required
					id="email"
					label="Почта"
					type="email"
					variant="filled"
					style={{ marginBottom: "1rem"}}
				/>

				<TextField
					onInput={(e) => setEducationOrganization(e.target.value)}
					required
					id="educationOrganization"
					label="Учебное заведение"
					type="text"
					variant="filled"
					style={{ marginBottom: "1rem"}}
				/>


				<LoadingButton
					loading={isLoading}
					type={"submit"}
					loadingPosition="start"
					startIcon={<SaveIcon />}
					variant="outlined"
				>
					Сохранить
				</LoadingButton>

			</form>

		</div>
	)
}
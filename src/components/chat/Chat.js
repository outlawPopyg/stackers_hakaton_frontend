import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Textarea from '@mui/joy/Textarea';
import IconButton from '@mui/joy/IconButton';
import Menu from '@mui/joy/Menu';
import MenuItem from '@mui/joy/MenuItem';
import ListItemDecorator from '@mui/joy/ListItemDecorator';
import FormatBold from '@mui/icons-material/FormatBold';
import FormatItalic from '@mui/icons-material/FormatItalic';
import KeyboardArrowDown from '@mui/icons-material/KeyboardArrowDown';
import Check from '@mui/icons-material/Check';
import Typography from "@mui/material/Typography";
import {Avatar, List, ListItem, ListItemContent} from "@mui/joy";
import {useSelector} from "react-redux";

export default function TextareaValidator() {
	const [italic, setItalic] = useState(false);
	const [fontWeight, setFontWeight] = useState('normal');
	const [anchorEl, setAnchorEl] = useState(null);
	const {id} = useParams();
	const [message, setMessage] = useState();
	const [data, setData] = useState([]);
	const user = useSelector(state => state.users);

	useEffect(() => {
		fetch(`http://localhost:8080/generalChatMessages/${id}`)
			.then(res => res.json())
			.then(data => setData(data));

	}, []);

	const handleSendMessage = (e) => {
		e.preventDefault();

		fetch("http://localhost:8080/saveMessage", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({author_id: user.id, event_id: id, message, type: 1})
		}).then(response => response.json())
			.then(data => {
				console.log(data);
				setData(prevState => [...prevState, data])
			}).catch(e => {

		})
	}


	return (
		<div>
			<h1>Общий чат</h1>

			<Box sx={{ width: 320 }}>
				<Typography
					id="ellipsis-list-demo"
					level="body4"
					textTransform="uppercase"
					fontWeight="xl"
					mb={1}
					sx={{ letterSpacing: '0.15rem' }}
				>
					Inbox
				</Typography>
				<List
					aria-labelledby="ellipsis-list-demo"
					sx={{ '--List-decorator-size': '56px' }}
				>
					{
						data.map(comment => {
							return (
								<ListItem key={comment.id}>
									<ListItemDecorator sx={{ alignSelf: 'flex-start' }}>
										<Avatar src="/static/images/avatar/1.jpg" />
									</ListItemDecorator>
									<ListItemContent>
										<Typography>{comment.author}</Typography>
										<Typography level="body2" noWrap>
											{comment.message}
										</Typography>
									</ListItemContent>
								</ListItem>
							)
						})
					}

				</List>
			</Box>

			{ user.surname !== "" ? (
				<form onSubmit={handleSendMessage}>
					<FormControl style={{ width: "25%"}}>
						<FormLabel>Введите сообщение</FormLabel>

						<Textarea
							onInput={(e) => setMessage(e.target.value)}
							placeholder="Type something here…"
							minRows={3}
							endDecorator={
								<Box
									sx={{
										display: 'flex',
										gap: 'var(--Textarea-paddingBlock)',
										pt: 'var(--Textarea-paddingBlock)',
										borderTop: '1px solid',
										borderColor: 'divider',
										flex: 'auto',
									}}
								>
									<IconButton
										variant="plain"
										color="neutral"
										onClick={(event) => setAnchorEl(event.currentTarget)}
									>
										<FormatBold />
										<KeyboardArrowDown fontSize="md" />
									</IconButton>
									<Menu
										anchorEl={anchorEl}
										open={Boolean(anchorEl)}
										onClose={() => setAnchorEl(null)}
										size="sm"
										placement="bottom-start"
										sx={{ '--List-decorator-size': '24px' }}
									>
										{['200', 'normal', 'bold'].map((weight) => (
											<MenuItem
												key={weight}
												selected={fontWeight === weight}
												onClick={() => {
													setFontWeight(weight);
													setAnchorEl(null);
												}}
												sx={{ fontWeight: weight }}
											>
												<ListItemDecorator>
													{fontWeight === weight && <Check fontSize="sm" />}
												</ListItemDecorator>
												{weight === '200' ? 'lighter' : weight}
											</MenuItem>
										))}
									</Menu>
									<IconButton
										variant={italic ? 'soft' : 'plain'}
										color={italic ? 'primary' : 'neutral'}
										aria-pressed={italic}
										onClick={() => setItalic((bool) => !bool)}
									>
										<FormatItalic />
									</IconButton>
									<Button type={"submit"} >Send</Button>
								</Box>
							}
							sx={{
								minWidth: 300,
								fontWeight,
								fontStyle: italic ? 'italic' : 'initial',
							}}
						/>
					</FormControl>
				</form>
			) : "" }

		</div>

	);
}
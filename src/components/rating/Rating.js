
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";

import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

export default function Rating(props) {
	const user = useSelector(state => state.users);
	const redirect = useNavigate();
	const [rating, setRating] = useState();

	useEffect(() => {
		if (user.email !== "") {
			fetch("http://localhost:8080/rating/" + user.id)
				.then(res => res.json()).then(res => setRating(res));
		}

	}, []);


	return (
		<div>
			{rating !== undefined ? <Alert severity="info">Вы на {rating.place} месте со счетом {rating.score}</Alert> :
				<Alert severity="error">Вы не авторизованы</Alert>}
		</div>
	)
}
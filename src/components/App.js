import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setName} from "../store/user/userReducer";


export default function App(props) {

	const dispatch = useDispatch();
	const data = useSelector(state => state.users);



	return (
		<div>
			<h1>{data.name === "" ? "Anonymous" : data.name}</h1>
			<button onClick={() => dispatch(setName("Kalim"))}>log in</button>
		</div>
	)
};
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './sidebar.css';

export default class Sidebar extends Component {

	onToggleClick = () => {
		let sidebar = document.querySelector('.sidebar');
		sidebar.classList.toggle('open');
	}

	componentDidMount() {
		let closeBtn = document.querySelector('#btn');
		closeBtn.addEventListener('click', this.onToggleClick)
	}

	createSidebarComponent(name, iconSelector, pathTo) {
		return (
			<li>
				<Link to={pathTo}>
					<i className={iconSelector} />
					<span className="links_name">{name}</span>
				</Link>
				<span className="tooltip">{name}</span>
			</li>
		);
	}

	render() {
		return (
			<>
				<div className="sidebar">
					<div className="logo-details">
						<div className="logo_name">StackUnion</div>
						<i className="bx bx-menu" id="btn"></i>
					</div>
					<ul className="nav-list">
						{this.createSidebarComponent("Dashboard", "bx bx-grid-alt", "/")}
						{this.createSidebarComponent("Rating", "bx bx-bar-chart-alt-2", "/rating")}
						{this.createSidebarComponent("User", "bx bx-user", "/user")}
						{this.createSidebarComponent("Message", "bx bx-message", "/message")}
						{this.createSidebarComponent("Analytics", "bx bx-network-chart", "/analytics")}
						{this.createSidebarComponent("Events", "bx bxs-rocket", "/events")}
						<li className="profile">
							<div className="profile-details">
								<img
									src="https://img.freepik.com/free-photo/young-handsome-man-with-beard-isolated-keeping-arms-crossed-frontal-position_1368-132662.jpg?size=626&ext=jpg"
									alt="profileImg"/>
								<div className="name_job">
									<div className="name">Prem Shahi</div>
									<div className="job">Student</div>
								</div>
							</div>
							<button>
								<i className="bx bx-log-out" id="log_out"></i>
							</button>

						</li>
					</ul>
				</div>
			</>
		);
	}
}

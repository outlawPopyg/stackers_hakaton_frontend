import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {useState} from "react";
import TextField from "@mui/material/TextField";


export default function OutlinedCard({ name, description, id, startTime, finishTime, geoPosition }) {

	const user = useSelector(state => state.users);
	const redirect = useNavigate();

	return (
		<Box sx={{ minWidth: 275 }}>
			<Card variant="outlined">
				<React.Fragment>
					<CardContent>
						<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
							{name}
						</Typography>
						<Typography variant="h5" component="div">
							{description}
						</Typography>
						<Typography sx={{ mb: 1.5 }} color="text.secondary">
							{geoPosition}
						</Typography>
						<Typography variant="body2">
							начало { startTime }
							<br />
							конец { finishTime }
						</Typography>
					</CardContent>
					<CardActions>
						<Button onClick={() => {
							if (user.isAuth === false) {
								redirect(`/reg_course/${id}`)
							}
						}} size="small">Записаться</Button>

						<Button onClick={() => redirect(`/chats/general/${id}`)}>
							Общий чат
						</Button>
					</CardActions>
				</React.Fragment>
			</Card>
		</Box>
	);
}


import * as events from "events";

export const eventsReducer = (state = [], action) => {
	switch (action.type) {
		case "ADD_ALL":
			return action.payload;
		default:
			return state;
	}
}

export const addAll = (events) => ({ type: "ADD_ALL", payload: events});
import { combineReducers} from "redux";
import { userReducer } from "./user/userReducer";
import { eventsReducer } from "./events/eventsReducer";

export const rootReducer = combineReducers({
	users: userReducer,
	events: eventsReducer
});
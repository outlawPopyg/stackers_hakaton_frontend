const initState = {
	isAuth: false,
	role: "anonymous",
	name: "",
	surname: "",
	patronymic: "",
	educationOrganization: "",
	email: ""
};

export const userReducer = (state = initState, action) => {
	switch (action.type) {
		case "REGISTER":
			return state;
		default:
			return state;
		case "SET_NAME":
			return {... state, name : action.payload}
		case "SET_USER":
			return { ...state, ...action.payload};
	}
}

export const registerUser = (user) => ({ type: "REGISTER", payload: user });
export const setName = (name) => ({ type: "SET_NAME", payload: name});
export const setUser = (user) => ({ type: "SET_USER", payload: user});